class Furniture {
  int id;
  String name;
  double price;
  String sku;
  String description;
  String color;
  String material;
  String image;
  String idCategory;

  Furniture({
      this.id,
      this.name,
      this.price,
      this.sku,
      this.description,
      this.color,
      this.material,
      this.image,
      this.idCategory});
}
