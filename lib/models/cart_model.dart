
import 'package:flutter/foundation.dart';
import 'package:my_ikea_app/models/furniture.dart';

class CartModel extends ChangeNotifier{

  List<Furniture> myList = [];

  void addFurnitureToList(Furniture furniture) {
    myList.add(furniture);
    notifyListeners();
  }
}