import 'package:my_ikea_app/models/furniture.dart';

List<Furniture> furnitureList = [
  new Furniture(id: 1, name: "Lisa", image: 'images/adriana.jpg'),
  new Furniture(id: 2, name: "Marta", image: 'images/boss.jpg'),
  new Furniture(id: 3, name: "Giuditta", image: 'images/jazz.jpg'),
  new Furniture(id: 4, name: "Giulia", image: 'images/rex.jpg'),
  new Furniture(id: 5, name: "Anna", image: 'images/virginia.jpg'),
  new Furniture(id: 6, name: "Federica", image: 'images/virginia.jpg'),
  new Furniture(id: 7, name: "Nicole", image: 'images/zurigo.jpg'),
];


class FurnitureService {

  List<Furniture> fetchAll() {
   return furnitureList;
  }

}

