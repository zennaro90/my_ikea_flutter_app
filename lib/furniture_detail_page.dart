import 'package:flutter/material.dart';
import 'package:my_ikea_app/models/cart_model.dart';
import 'package:my_ikea_app/models/furniture.dart';
import 'package:provider/provider.dart';

class FurnitureDetailPage extends StatelessWidget {
  final Furniture furniture;

  FurnitureDetailPage({@required this.furniture});



  @override
  Widget build(BuildContext context) {

    void onClick() {
      CartModel cart = Provider.of<CartModel>(context);
      cart.addFurnitureToList(furniture);
    }

    Widget updateCartLabel(context, cart, child) {
      return Text('Pino: ${cart.myList.length}' );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Dettaglio"),
        actions: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.shopping_cart),
              Consumer<CartModel>(
                builder: updateCartLabel,
              ),
              SizedBox(width: 10,)
            ],
          )
        ],
      ),
      body: Center(
        child: Text("Questa è la pagina di dettaglio del mobile ${this.furniture.name}"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: onClick,
        child: Icon(Icons.add_shopping_cart),
      ),
    );
  }
}
